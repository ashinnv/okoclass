package main

import(
	"gitlab.com/ashinnv/okonet"
	"gitlab.com/ashinnv/okoframe"
)

/*
Okokit stage to classify images.
*/

func main(){

	for i := 0; i < classifierCount; i++{
		go classify(stageOne, finchan, classPath)
	}

	go okonet.FrameListen(listenTarget, stageOne)
	go okonet.FrameSender(sendoffTarget, finchan)

}

func classify(input chan okoframe.Frame, output chan okoframe.Frame, classpath string){

	classifier := gocv.NewCascadeClassifier()
	defer classifier.Close()

	if !classifier.Load(classpath){
		fmt.Println("ERROR READING CLASSIFIER FILE:", classpath)
		return
	}

	for {
		tmpFrame <- input

		//Simple classification taken from the examples on gocv.io
		tmpMat := okoframe.FrameToMat(tmpFrame)
		tmpFrame.MotionLocations = classifier.DetectMultiScale(tmpMat)

		output <- tmpFrame
	}

}